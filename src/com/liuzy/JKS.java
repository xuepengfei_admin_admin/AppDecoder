package com.liuzy;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.OutputStreamWriter;
import java.security.KeyStore;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.cert.Certificate;
import java.security.cert.CertificateFactory;

/**
 * KetStore操作
 * 
 * @author liuzy
 * @since 2015年11月1日
 */
public class JKS {
	/**
	生成keystore
	keytool -genkey -alias liuzy -keypass 111111 -keyalg RSA -keysize 1024 -validity 3650 -keystore liuzy.jks -storepass 111111 -dname "CN=liuzy, OU=com, O=com, L=shanghai, ST=shanghai, C=cn"
	
	查看keystore
	keytool -list  -v -keystore liuzy.jks -storepass 111111
	
	导出证书
	keytool -export -alias liuzy -keystore liuzy.jks -file liuzy.cer -storepass 111111
	
	查看证书
	keytool -printcert -file liuzy.cer
	 */
	public static void main(String[] args) {
		readJKS("D:\\liuzy.jks", "111111", "liuzy");
		writeKey("D:\\liuzy.key");
		readCer("D:\\liuzy.cer");
	}
	private static PublicKey publicKey;
	private static PrivateKey privateKey;
	/** 读jks公钥和私钥 */
	public static void readJKS(String jks, String jksPwd, String alias) {
		try {
			FileInputStream is = new FileInputStream(jks);
			KeyStore ks = KeyStore.getInstance("JKS");
			ks.load(is, jksPwd.toCharArray());
			is.close();
			publicKey = ks.getCertificate(alias).getPublicKey();
			privateKey =  (PrivateKey) ks.getKey(alias, jksPwd.toCharArray());
			show();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	public static void show() {
		byte[] publicBytes = publicKey.getEncoded();
		System.out.println("publicBytes " + publicBytes.length);
		String pulicHex = Util.bytesToHexStr(publicBytes);
		System.out.println("publicHex " + pulicHex.length());
		System.out.println(pulicHex);
		String publicBase64 = Base64.byteArrayToBase64(publicBytes);
		System.out.println("publicBase64 " + publicBase64.length());
		System.out.println(publicBase64);

		byte[] privateBytes = privateKey.getEncoded();
		System.out.println("privateBytes " + privateBytes.length);
		String privateHex = Util.bytesToHexStr(privateBytes);
		System.out.println("privateHex " + privateHex.length());
		System.out.println(privateHex);
		String privateBase64 = Base64.byteArrayToBase64(privateBytes);
		System.out.println("privateBase64 " + privateBase64.length());
		System.out.println(privateBase64);
	}
	public static void writeKey(String keyFile) {
//		byte[] privateBytes = privateKey.getEncoded();
//		byte[] rsaBytes = new byte[privateBytes.length - 26];
//		System.arraycopy(privateBytes, 26, rsaBytes, 0, rsaBytes.length);
		String privateBase64 = Base64.byteArrayToBase64(privateKey.getEncoded());
		try {
			FileOutputStream os = new FileOutputStream(keyFile);
			OutputStreamWriter sw = new OutputStreamWriter(os);
			sw.append("-----BEGIN RSA PRIVATE KEY-----\n");
			int i = 0;
			for (char ch : privateBase64.toCharArray()) {
				sw.append(ch);
				if (i++ == 63) {
					i = 0;
					sw.append('\n');
				}
			}
			sw.append("\n-----END RSA PRIVATE KEY-----");
			sw.flush();
			sw.close();
			os.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	/** 读证书文件打印公钥 */
	public static void readCer(String cerFile) {
		try {
			CertificateFactory of = CertificateFactory.getInstance("x.509");
			FileInputStream is = new FileInputStream(cerFile);
			Certificate cer = of.generateCertificate(is);
			is.close();
			PublicKey publicKey = cer.getPublicKey();
			byte[] publicBytes = publicKey.getEncoded();
			System.out.println("publicBytes " + publicBytes.length);
			String pulicHex = Util.bytesToHexStr(publicBytes);
			System.out.println("publicHex " + pulicHex.length());
			System.out.println(pulicHex);
			String publicBase64 = Base64.byteArrayToBase64(publicBytes);
			System.out.println("publicBase64 " + publicBase64.length());
			System.out.println(publicBase64);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
