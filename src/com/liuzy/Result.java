package com.liuzy;

public class Result {
	private String appName;
	private String packageName;
	private String version;
	public String getAppName() {
		return appName;
	}
	public void setAppName(String appName) {
		this.appName = appName;
	}
	public String getPackageName() {
		return packageName;
	}
	public void setPackageName(String packageName) {
		this.packageName = packageName;
	}
	public String getVersion() {
		return version;
	}
	public void setVersion(String version) {
		this.version = version;
	}
	@Override
	public String toString() {
		return "appName=" + appName + "\npackageName=" + packageName + "\nversion=" + version;
	}
}
